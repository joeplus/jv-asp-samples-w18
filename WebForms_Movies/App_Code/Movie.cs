﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Movie
/// </summary>
public class Movie
{
    public string title;
    public DateTime date;
    public string theatre;
    public int rating;

    public Movie(string title, DateTime date, string theatre, int rating)
    {
        this.title = title;
        this.date = date;
        this.theatre = theatre;
        this.rating = rating;
    }
}