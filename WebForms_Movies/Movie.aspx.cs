﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Movie : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        TitleTextBox.Focus();
    }

    protected void RatingTextBox_TextChanged(object sender, EventArgs e)
    {

    }

    protected void AddButton_Click(object sender, EventArgs e)
    {
        ResultLtl.Text = $"<b>{TitleTextBox.Text} {DateTextBox.Text} {TheatreTextBox.Text} {RatingTextBox.Text}"
                            + " was added!</b>";
        ResultLbl.Text = ResultLtl.Text;
    }
}