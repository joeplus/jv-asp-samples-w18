﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            NameTB.Focus();
            ViewState["Greeting"] = "Welcome to our site.  No postback Page_Load!";
        }
        else 
            ViewState["Greeting"] = " Welcome BACK to this page.  This is a postback Page_Load!";

        if (Session["Count"] == null)
            Session["Count"] = 1;
        else
            Session["Count"] = (int)Session["Count"] + 1;

        if (ViewState["Count"] == null)
            ViewState["Count"] = 1;
        else
            ViewState["Count"] = (int)ViewState["Count"] + 1;

        SessionCntLbl.Text = $"Session count: {Session["Count"]}";
        ViewStateCntLbl.Text = $"ViewState count: {ViewState["Count"]}";
    }

    protected void SubmitBtn_Click(object sender, EventArgs e)
    {
        //GreetingLbl.Text = "Hello " + NameTB.Text + "!" + ViewState["Greeting"] + "You clicked the submit button";
        GreetingLbl.Text = $"Hello {NameTB.Text}! {ViewState["Greeting"]} You clicked the submit button";
    }

    protected void NameTB_TextChanged(object sender, EventArgs e)
    {

    }

    protected void PasswordCV_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (args.Value.Length <= 8)
        {
            args.IsValid = false;
            return;
        }

        bool upperCase = false;
        foreach (Char c in args.Value) { 
            if (char.IsUpper(c))
                upperCase = true;
            break;
        }
        if (!upperCase)
          args.IsValid = false;
    }
}