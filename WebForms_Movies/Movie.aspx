﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Movie.aspx.cs" Inherits="Movie" Title="Web Forms - Movie Tracker" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    </head>
<body>
    <form id="form1" runat="server" defaultbutton="AddButton">
        <div>
            <h1>Movie Tracker</h1>

    <table>
        <tr>
            <td>Title</td>
            <td>
                <asp:TextBox ID="TitleTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Date Seen</td>
            <td>
                <asp:TextBox ID="DateTextBox" runat="server" placeholder="YYYY-MM-DD"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Theatre</td>
            <td>
                <asp:TextBox ID="TheatreTextBox" runat="server" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Rating</td>
            <td>
                <asp:TextBox ID="RatingTextBox" runat="server" OnTextChanged="RatingTextBox_TextChanged" placeholder="1 to 10"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="AddButton" runat="server" OnClick="AddButton_Click" Text="Add Movie" />
            </td>
            <td></td>
        </tr>
    </table>
            <asp:Literal ID="ResultLtl" runat="server"></asp:Literal>
            <br />
            <asp:Label ID="ResultLbl" runat="server" CssClass="Label" Font-Bold="False" Font-Italic="False"></asp:Label>
            <br />
        </div>
    </form>
</body>
</html>
