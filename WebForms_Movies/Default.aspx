﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" Title="Web Forms - Movie Tracker" MasterPageFile="MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div>
            <h1>Standard Controls</h1>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2">
            <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Name:"></asp:Label>
                    </td>
                    <td class="auto-style3">
            <asp:TextBox ID="NameTB" runat="server" OnTextChanged="NameTB_TextChanged"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="NameTB" CssClass="Error" ErrorMessage="Please enter your name"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
            <asp:Label ID="Label2" runat="server" CssClass="Label" Text="Country"></asp:Label>
                    </td>
                    <td class="auto-style3">
            <asp:DropDownList ID="CountryDDL" runat="server" AutoPostBack="True">
                <asp:ListItem>Please select a country</asp:ListItem>
                <asp:ListItem>Canada</asp:ListItem>
                <asp:ListItem>USA</asp:ListItem>
                <asp:ListItem>Mexico</asp:ListItem>
                <asp:ListItem>Cuba</asp:ListItem>
            </asp:DropDownList>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="CountryDDL" CssClass="Error" ErrorMessage="Please select a country" InitialValue="Please select a country"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
            <asp:Label ID="Label3" runat="server" CssClass="Label" Text="DOB:"></asp:Label>
                    </td>
                    <td class="auto-style3">
            <asp:TextBox ID="DOBTB" runat="server" TextMode="Date"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="DOBTB" CssClass="Error" Display="Dynamic" ErrorMessage="Please enter your DOB"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="DOBTB" CssClass="Error" Display="Dynamic" ErrorMessage="Enter a valid date" Operator="GreaterThan" Type="Date" ValueToCompare="1880-01-01"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
            <asp:Label ID="Label4" runat="server" CssClass="Label" Text="Color:"></asp:Label>
                    </td>
                    <td class="auto-style3">
            <asp:TextBox ID="ColorTB" runat="server" TextMode="Color"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ColorTB" CssClass="Error" ErrorMessage="Please enter a color"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
            <asp:Label ID="Label5" runat="server" CssClass="Label" Text="Password:"></asp:Label>
                    </td>
                    <td class="auto-style3">
            <asp:TextBox ID="PasswordTB" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="PasswordTB" CssClass="Error" Display="Dynamic" ErrorMessage="Please enter a password"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="PasswordCV" runat="server" ControlToValidate="PasswordTB" CssClass="Error" Display="Dynamic" ErrorMessage="Password must be at least 8 character and have one upper case..." OnServerValidate="PasswordCV_ServerValidate"></asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
            <asp:Label ID="Label6" runat="server" CssClass="Label" Text="Email:"></asp:Label>
                    </td>
                    <td class="auto-style3">
            <asp:TextBox ID="EmailTB" runat="server" TextMode="Email"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="EmailTB" CssClass="Error" Display="Dynamic" ErrorMessage="Please enter an email address"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="EmailTB" CssClass="Error" Display="Dynamic" ErrorMessage="Email address is invalid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
            <asp:Label ID="Label7" runat="server" CssClass="Label" Text="Phone:"></asp:Label>
                    </td>
                    <td class="auto-style3">
            <asp:TextBox ID="PhoneTB" runat="server" TextMode="Phone"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="PhoneTB" CssClass="Error" Display="Dynamic" ErrorMessage="Please enter a phone number"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="PhoneTB" CssClass="Error" Display="Dynamic" ErrorMessage="Phone number is invalid" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
            <asp:Label ID="Label8" runat="server" CssClass="Label" Text="Rating:"></asp:Label>
                    </td>
                    <td class="auto-style3">
            <asp:TextBox ID="RatingTB" runat="server" TextMode="Range"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>

            <asp:Button ID="SubmitBtn" runat="server" OnClick="SubmitBtn_Click" Text="Submit" />

            <br />
            <asp:Label ID="GreetingLbl" runat="server" Text="Hello World!" CssClass="Label"></asp:Label>

            <br />
            <asp:Label ID="SessionCntLbl" runat="server" CssClass="Label"></asp:Label>

            <br />
            <asp:Label ID="ViewStateCntLbl" runat="server" CssClass="Label"></asp:Label>

            <br />
        </div>
</asp:Content>
