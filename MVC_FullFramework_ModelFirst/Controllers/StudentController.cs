﻿using MVC_FullFramework_ModelFirst.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MVC_FullFramework_ModelFirst.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            try
            {
                using (CollegeDB db = new CollegeDB())
                {
                    if (db.Students.Count() == 0)
                        return HttpNotFound();
                    return View(db.Students.ToList());
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Failed to fetch the list of students in the Index Action of the Student Controller " + 
                                 $"at: {DateTime.Now} due to exception: {ex.Message}");
                return View("Error");
            }
        }

        public ActionResult SeedDB()
        {
            try
            {
                using (CollegeDB db = new CollegeDB())
                {
                    if (db.Students.Count() == 0)
                    {
                        Course course = new Course { Code = "YYZ", Name = "SWEng" };
                        db.Courses.Add(course);
                        db.SaveChanges();

                        Student student = new Student { FirstName = "Jesse", LastName = "James", StudentNumber = "654321", DateOfBirth = DateTime.Now.AddYears(-25) };
                        db.Students.Add(student);
                        db.SaveChanges();

                        student.Courses.Add(course);
                        db.SaveChanges();

                        //return new HttpStatusCodeResult(HttpStatusCode.Created);
                        return RedirectToAction(nameof(Index));
                    }
                }
                //return new HttpStatusCodeResult(HttpStatusCode.OK);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Failed to seed the College database in the SeedDB Action of the Student Controller " +
                                 $"at: {DateTime.Now} due to exception: {ex.Message}");
                return View("Error");
            }
        }
    }
}