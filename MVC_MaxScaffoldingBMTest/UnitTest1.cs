using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVC_MaxScaffolding.Controllers;
using MVC_MaxScaffolding.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MVC_MaxScaffoldingTest
{
    [TestClass]
    public class UnitTest1
    {
        private static CollegeContext collegeTestContext;
        [ClassInitialize]
        public static void Initialize(TestContext tc)
        {
            var options = new DbContextOptionsBuilder<CollegeContext>()
                   .UseInMemoryDatabase(databaseName: "StudentControllerTests").Options;
            collegeTestContext = new CollegeContext(options);

            POS pos1 = new POS { Id = 1, Code = "558", Name = "SWS", Description = "2 year Diploma" };
            POS pos2 = new POS { Id = 2, Code = "559", Name = "SWD", Description = "3 year Advanced Diploma" };
            collegeTestContext.POSs.AddRange(pos1, pos2);
            collegeTestContext.SaveChanges();

            Student student1 = new Student { Id = 1, FirstName = "Chris", LastName = "Bosh", StudentNumber = "12345",
                                             BirthDate = DateTime.Now.AddYears(-20), POS = pos1 };
            Student student2 = new Student { Id = 2, FirstName = "Faith", LastName = "Hope", StudentNumber = "54321",
                                             BirthDate = DateTime.Now.AddYears(-21), POS = pos2 };
            collegeTestContext.Students.AddRange(student1, student2);
            collegeTestContext.SaveChanges();
        }


        [TestMethod]
        public async Task StudentIndexText()
        {
            // Arrange
            StudentController sc = new StudentController(collegeTestContext);

            //Act
            IActionResult result = await sc.Index();

            //Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            ViewResult vr = result as ViewResult;
            Assert.IsInstanceOfType(vr.Model, typeof(List<Student>));
            List<Student> students = vr.Model as List<Student>;
            Assert.AreEqual(2, students.Count);
            Assert.AreEqual(1, students[0].Id);
            Assert.AreEqual("12345", students[0].StudentNumber);
            Assert.AreEqual(DateTime.Now.AddYears(-20).Date, students[0].BirthDate.Date);
            Assert.AreEqual("Chris", students[0].FirstName);
            Assert.AreEqual("Hope", students[1].LastName);
            Assert.AreEqual(2, students[1].POSId);
            Assert.AreEqual("SWD", students[1].POS.Name);
        }
    }
}
