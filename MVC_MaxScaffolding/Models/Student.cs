﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_MaxScaffolding.Models
{
    public class Student
    {
        [Key]
        public int Id { get; set; }
        [Required, Display(Name ="Student Number")]
        public string StudentNumber { get; set; }
        [Required, Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required, Display(Name = "Last Name")]
        public string LastName { get; set; }
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }
        [ForeignKey("POS")]
        public int POSId { get; set; }
        public virtual POS POS { get; set; }
    }
}
