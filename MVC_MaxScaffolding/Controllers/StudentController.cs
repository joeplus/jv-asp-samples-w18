﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MVC_MaxScaffolding.Models;

namespace MVC_MaxScaffolding.Controllers
{
    public class StudentController : Controller
    {
        private readonly CollegeContext _context;

        public StudentController(CollegeContext context)
        {
            _context = context;
        }

        // GET: Student
        public async Task<IActionResult> Index()
        {
            var collegeContext = _context.Students.Include(s => s.POS);
            return View(await collegeContext.ToListAsync());
        }

        // GET: Student/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Students
                .Include(s => s.POS)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // GET: Student/Create
        public IActionResult Create()
        {
            ViewData["POSId"] = new SelectList(_context.Set<POS>(), "Id", "Code");
            return View();
        }

        // POST: Student/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FirstName,LastName,StudentNumber,BirthDate,POSId")] Student student)
        {
            if (ModelState.IsValid)
            {
                _context.Add(student);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["POSId"] = new SelectList(_context.Set<POS>(), "Id", "Code", student.POSId);
            return View(student);
        }

        // GET: Student/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Students.SingleOrDefaultAsync(m => m.Id == id);
            if (student == null)
            {
                return NotFound();
            }
            ViewData["POSId"] = new SelectList(_context.Set<POS>(), "Id", "Code", student.POSId);
            return View(student);
        }

        // POST: Student/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FirstName,LastName,StudentNumber,BirthDate,POSId")] Student student)
        {
            if (id != student.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(student);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StudentExists(student.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["POSId"] = new SelectList(_context.Set<POS>(), "Id", "Code", student.POSId);
            return View(student);
        }

        // GET: Student/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Students
                .Include(s => s.POS)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // POST: Student/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var student = await _context.Students.SingleOrDefaultAsync(m => m.Id == id);
            _context.Students.Remove(student);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StudentExists(int id)
        {
            return _context.Students.Any(e => e.Id == id);
        }

        public IActionResult SeedDB()
        {
            if (_context.Students.Count() == 0)
            {
                POS pos1 = new POS { Code = "558", Name = "SWS", Description = "2 Year Diploma" };
                POS pos2 = new POS { Code = "559", Name = "SWD", Description = "3 Year Advanced Diploma" };
                _context.POSs.AddRange(pos1, pos2);
                _context.SaveChanges();

                Student student1 = new Student { FirstName = "LeBron", LastName = "James", StudentNumber = "12345", BirthDate = DateTime.Now.AddYears(-20), POS = pos1 };
                Student student2 = new Student { FirstName = "Taylor", LastName = "Swift", StudentNumber = "22222", BirthDate = DateTime.Now.AddYears(-21), POS = pos2 };
                _context.Students.AddRange(student1, student2);
                _context.SaveChanges();
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
