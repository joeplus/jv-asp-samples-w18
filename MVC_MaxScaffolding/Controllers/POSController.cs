﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MVC_MaxScaffolding.Models;

namespace MVC_MaxScaffolding.Controllers
{
    public class POSController : Controller
    {
        private readonly CollegeContext _context;

        public POSController(CollegeContext context)
        {
            _context = context;
        }

        // GET: POS
        public async Task<IActionResult> Index()
        {
            return View(await _context.POSs.ToListAsync());
        }

        // GET: POS/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pOS = await _context.POSs
                .SingleOrDefaultAsync(m => m.Id == id);
            if (pOS == null)
            {
                return NotFound();
            }

            return View(pOS);
        }

        // GET: POS/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: POS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Code,Name,Description")] POS pOS)
        {
            if (ModelState.IsValid)
            {
                _context.Add(pOS);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(pOS);
        }

        // GET: POS/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pOS = await _context.POSs.SingleOrDefaultAsync(m => m.Id == id);
            if (pOS == null)
            {
                return NotFound();
            }
            return View(pOS);
        }

        // POST: POS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Code,Name,Description")] POS pOS)
        {
            if (id != pOS.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(pOS);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!POSExists(pOS.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(pOS);
        }

        // GET: POS/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pOS = await _context.POSs
                .SingleOrDefaultAsync(m => m.Id == id);
            if (pOS == null)
            {
                return NotFound();
            }

            return View(pOS);
        }

        // POST: POS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var pOS = await _context.POSs.SingleOrDefaultAsync(m => m.Id == id);
            _context.POSs.Remove(pOS);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool POSExists(int id)
        {
            return _context.POSs.Any(e => e.Id == id);
        }
    }
}
