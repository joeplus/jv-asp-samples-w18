﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MVC_MaxScaffolding.Models
{
    public class CollegeContext : DbContext
    {
        public CollegeContext (DbContextOptions<CollegeContext> options)
            : base(options)
        {
        }

        public DbSet<MVC_MaxScaffolding.Models.Student> Students { get; set; }
        public DbSet<MVC_MaxScaffolding.Models.POS> POSs { get; set; }
    }
}
