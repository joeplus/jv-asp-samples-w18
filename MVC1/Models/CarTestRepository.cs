﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC1.Models
{
    public class CarTestRepository : ICarRepository
    {
        private static List<Car> myCars = new List<Car> {
                                                   new Car { Id = 1, Make = "Toyota", Model= "Camry", Year = 2015, Email = "camry@toyota.com"},
                                                   new Car { Id = 2, Make = "Honda", Model= "Accord", Year = 2016, Email = "accord@honda.com"},
                                                   new Car { Id = 3, Make = "Chevy", Model= "Malibu", Year = 2017, Email = "malibu@chevy.com"}
                                            };

        public Car GetCar(int id)
        {
            int index = myCars.FindIndex(c => c.Id == id);
            if (index < 0)
                return null;
            return myCars[index];
        }

        public IEnumerable<Car> GetCars()
        {
            return myCars;
        }
    }
}
