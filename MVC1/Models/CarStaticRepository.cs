﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC1.Models
{
    public class CarStaticRepository : ICarRepository
    {
        private static List<Car> myCars = new List<Car> {
                                                   new Car { Id = 1, Make = "Toyota", Model= "Camry", Year = 2015, Email = "camry@toyota.com"},
                                                   new Car { Id = 2, Make = "Toyota", Model= "Corolla", Year = 2016, Email = "corolla@toyota.com"},
                                                   new Car { Id = 3, Make = "Toyota", Model= "RAV4", Year = 2017}
                                            };

        public Car GetCar(int id)
        {
            int index = myCars.FindIndex(c => c.Id == id);
            if (index < 0)
                return null;
            return myCars[index];
        }

        public IEnumerable<Car> GetCars()
        {
            return myCars;
        }
    }
}
