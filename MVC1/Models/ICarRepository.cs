﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC1.Models
{
    public interface ICarRepository
    {
        IEnumerable<Car> GetCars();
        Car GetCar(int id);
    }
}
