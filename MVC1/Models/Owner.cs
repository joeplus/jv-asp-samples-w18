﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MVC1.Models
{
    public class Owner
    {
        [Key]
        public int Id { get; set; }
        [Required, Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required, Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name ="Birthdate"), DataType(DataType.DateTime)]
        public DateTime BirthDate { get; set; }
    }
}
