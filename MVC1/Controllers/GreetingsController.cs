﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MVC1.Controllers
{
    public class GreetingsController : Controller
    {
        public GreetingsController() : base() { }

        public IActionResult Index()
        {
            ViewData["VDTS"] = DateTime.Now;
            ViewBag.VBTS = DateTime.Now;
            return View();
        }

        public string Hello(string name)
        {
            return $"Hello {name}! Welcome to ASP.NET MVC!";
        }
    }
}