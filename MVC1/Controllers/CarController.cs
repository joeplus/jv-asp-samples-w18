﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MVC1.Models;

namespace MVC1.Controllers
{
    public class CarController : Controller
    {
        private static List<Car> myCars = new List<Car> {
                                                   new Car { Id = 1, Make = "Toyota", Model= "Corolla", Year = 2015, Email = "corolla@toyota.com"},
                                                   new Car { Id = 2, Make = "Toyota", Model= "Camry", Year = 2016, Email = "camry@toyota.com"},
                                                   new Car { Id = 3, Make = "Toyota", Model= "RAV4", Year = 2017}
                                            };
        private static List<Owner> myOwners = new List<Owner> {
                                                   new Owner { Id = 1, FirstName = "LeBron", LastName= "James"},
                                                   new Owner { Id = 2, FirstName = "Taylor", LastName= "Swift"},
                                                   new Owner { Id = 3, FirstName = "Tessa", LastName= "Virtue"},
                                            };

        // GET: Car
        public ActionResult Index()
        {
            return View(myCars);
        }

        // GET: Car/Details/5
        public ActionResult Details(int id)
        {
            int index = myCars.FindIndex(c => c.Id == id);
            if (index >= 0)
            {
                Owner theOwner = null;
                if (index < myOwners.Count)
                    theOwner = myOwners[index];
                Car theCar = myCars[index];

                // Debug Builds only
                string msg = $"Details ID is {id}";
                Debug.Write(msg);
                string indexMsg = $"The index is: {index}";
                Debug.WriteLineIf(id == 2, indexMsg);
                //Debug.Assert(index >= 1, "Details index is zero!");

                // Debug + Release builds
                Trace.Write("Trace.Write:" + msg);
                indexMsg = $"Trace.WriteLineIf: The index is: {index}";
                Trace.WriteLineIf(id == 2, indexMsg);
                //Trace.Assert(index >= 1, "Trace.Assert: Details index is zero!");

                VMCar theVMCar = new VMCar
                {
                    Id = theCar.Id,
                    Email = theCar.Email,
                    Make = theCar.Make,
                    Model = theCar.Model,
                    Year = theCar.Year
                };
                if (theOwner != null)
                    theVMCar.FullName = $"{theOwner.FirstName} {theOwner.LastName}";
                else
                    theVMCar.FullName = "Not found";
                return View(theVMCar);
            }
            else
                return View("Error", new ErrorViewModel
                {
                    RequestId = "Car Details ID: " + id.ToString(),
                    Description = "Car Id not found"
                });
        }

        // GET: Car/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Car/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Car newCar)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    int id;
                    if (myCars.Count == 0)
                        id = 1;
                    else
                        id = myCars.Max(c => c.Id) + 1;
                    newCar.Id = id;
                    myCars.Add(newCar);
                    return RedirectToAction(nameof(Index));
                }
                else
                    return View();
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel
                                        { RequestId = "Car Create",
                                          Description = "Exception creating new car due to: " + ex.Message});
            }
        }

        // GET: Car/Edit/5
        public ActionResult Edit(int id)
        {
            int index = myCars.FindIndex(c => c.Id == id);
            if (index >= 0)
                return View(myCars[index]);
            else
                return View("Error", new ErrorViewModel
                {
                    RequestId = "Car Edit ID: " + id.ToString(),
                    Description = "Car Id not found"
                });
        }

        // POST: Car/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Car updatedCar)
        {
            try
            {
                // TODO: Add update logic here
                if (ModelState.IsValid)
                {
                    int index = myCars.FindIndex(c => c.Id == id);
                    myCars[index] = updatedCar;
                    return RedirectToAction(nameof(Index));
                }
                else
                    return View();
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel
                {
                    RequestId = "Car Edit Post",
                    Description = "Exception editing existing car due to: " + ex.Message
                });
            }
        }

        // GET: Car/Delete/5
        public ActionResult Delete(int id)
        {
            int index = myCars.FindIndex(c => c.Id == id);
            if (index >= 0)
                return View(myCars[index]);
            else
                return View("Error", new ErrorViewModel
                {
                    RequestId = "Car Delete ID: " + id.ToString(),
                    Description = "Car Id not found"
                });
        }

        // POST: Car/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, Car car)
        {
            try
            {
                // TODO: Add delete logic here
                int index = myCars.FindIndex(c => c.Id == id);

                if (index >= 0) { 
                    myCars.RemoveAt(index);
                    return RedirectToAction(nameof(Index));
                }
                else
                    return View("Error", new ErrorViewModel
                    {
                        RequestId = "Car Delete Post ID: " + id.ToString(),
                        Description = "Car Id not found"
                    });
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel
                {
                    RequestId = "Car Delete Post",
                    Description = "Exception deleting new car due to: " + ex.Message
                });
            }
        }
    }
}