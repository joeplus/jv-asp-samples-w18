﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MVC1.Models;

namespace MVC1.Controllers
{
    public class CarRepoController : Controller
    {
        //private CarStaticRepository db;
        private ICarRepository db;

        //public CarRepoController() :base()
        public CarRepoController(ICarRepository repo) : base()
        {
            //db = new CarStaticRepository();
            db = repo;
        }

        // GET: CarRepo
        public ActionResult Index()
        {
            return View(db.GetCars());
        }

        // GET: CarRepo/Details/5
        public ActionResult Details(int id)
        {
            return View(db.GetCar(id));
        }
    }
}