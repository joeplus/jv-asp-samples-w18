using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVC1.Controllers;
using MVC1.Models;
using System.Collections.Generic;

namespace MVC1Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GreetingsControllerHelloActionTest()
        {
            // Arrange
            GreetingsController greetingsController = new GreetingsController();

            // Act
            string greeting = greetingsController.Hello("Sally");

            // Assert
            Assert.AreEqual("Hello Sally! Welcome to ASP.NET MVC!", greeting);
        }

        [TestMethod]
        public void CarControllerDetailsActionTest()
        {
            // Arrange
            CarController carController = new CarController();

            // Act
            ActionResult result = carController.Details(2);

            // Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            ViewResult vResult = result as ViewResult;
            Assert.IsInstanceOfType(vResult.Model, typeof(VMCar));
            VMCar vmCar = vResult.Model as VMCar;
            // Test all fields
            Assert.AreEqual(2, vmCar.Id);
            Assert.AreEqual("Toyota", vmCar.Make);
            Assert.AreEqual("Camry", vmCar.Model);
            Assert.AreEqual(2016, vmCar.Year);
            Assert.AreEqual("camry@toyota.com", vmCar.Email);
            Assert.AreEqual("Taylor Swift", vmCar.FullName);
        }
        [TestMethod]
        public void CarControllerIndexActionTest()
        {
            // Arrange
            CarController carController = new CarController();

            // Act
            ActionResult result = carController.Index();

            // Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            ViewResult vResult = result as ViewResult;
            Assert.IsInstanceOfType(vResult.Model, typeof(List<Car>));
            List<Car> myCars = vResult.Model as List<Car>;
            // Check the number of cars, a portion of every record and all fields
            Assert.AreEqual(3, myCars.Count);
            Assert.AreEqual(1, myCars[0].Id);
            Assert.AreEqual(2016, myCars[1].Year);
            Assert.AreEqual("Toyota", myCars[2].Make);
            Assert.AreEqual("RAV4", myCars[2].Model);
            Assert.AreEqual("RAV4@toyota.com", myCars[2].Email);
        }

        [TestMethod]
        public void CarRepoControllerIndexActionTest()
        {
            // Arrange
            CarTestRepository testDB = new CarTestRepository();
            CarRepoController carRController = new CarRepoController(testDB);

            // Act
            ActionResult result = carRController.Index();

            // Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            ViewResult vResult = result as ViewResult;
            Assert.IsInstanceOfType(vResult.Model, typeof(List<Car>));
            List<Car> myCars = vResult.Model as List<Car>;
            // Check the number of cars, a portion of every record and all fields
            Assert.AreEqual(3, myCars.Count);
            Assert.AreEqual(1, myCars[0].Id);
            Assert.AreEqual(2016, myCars[1].Year);
            Assert.AreEqual("Chevy", myCars[2].Make);
            Assert.AreEqual("Malibu", myCars[2].Model);
            Assert.AreEqual("malibu@chevy.com", myCars[2].Email);
        }
    }
}
