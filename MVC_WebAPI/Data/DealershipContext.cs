﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MVC_WebAPI.Models
{
    public class DealershipContext : DbContext
    {
        public DealershipContext (DbContextOptions<DealershipContext> options)
            : base(options)
        {
        }

        public DbSet<MVC_WebAPI.Models.Car> Cars { get; set; }
    }
}
