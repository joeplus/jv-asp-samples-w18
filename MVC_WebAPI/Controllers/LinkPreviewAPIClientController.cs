﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MVC_WebAPI.Models;

namespace MVC_WebAPI.Controllers
{
    public class LinkPreviewAPIClientController : Controller
    {
        // GET: APIClient
        public async Task<LinkPreview> Preview(string link)
        {
            LinkPreview retValue = new LinkPreview();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://api.linkpreview.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string URL = "?key=123456&q=" + link;
                HttpResponseMessage response = await client.GetAsync(URL);
                if (response.IsSuccessStatusCode)
                {
                    retValue = await response.Content.ReadAsAsync<LinkPreview>();
                }
            }
            return retValue;
        }

        public async Task<IActionResult> PreviewUsingView(string link)
        {
            LinkPreview retValue = new LinkPreview();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://api.linkpreview.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string URL = "?key=123456&q=" + link;
                HttpResponseMessage response = await client.GetAsync(URL);
                if (response.IsSuccessStatusCode)
                {
                    retValue = await response.Content.ReadAsAsync<LinkPreview>();
                }
            }
            return View(retValue);
        }

    }
}