using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVC_WebAPI.Controllers;
using MVC_WebAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MVC_WebAPI_Test
{
    [TestClass]
    public class UnitTest1
    {
        private static DealershipContext dealershipTestContext;
        [ClassInitialize]
        public static void Initialize(TestContext tc)
        {
            var options = new DbContextOptionsBuilder<DealershipContext>()
                                .UseInMemoryDatabase(databaseName: "DealershipTestDB").Options;
            dealershipTestContext = new DealershipContext(options);

            Car car1 = new Car { Id = 1, Make = "Chevy", Model = "Impala", Year = 2007 };
            Car car2 = new Car { Id = 2, Make = "Chevy", Model = "Malibu", Year = 2015 };
            dealershipTestContext.Cars.AddRange(car1, car2);
            dealershipTestContext.SaveChanges();
        }

        [TestMethod]
        public async Task CarControllerGetAllTest()
        {
            // Arrange
            CarApiController cc = new CarApiController(dealershipTestContext);

            // Act
            IEnumerable<Car> myCars = cc.GetCar();

            // Assert
            Assert.IsInstanceOfType(myCars, typeof(DbSet<Car>));
            DbSet<Car> dbCars = myCars as DbSet<Car>;
            Assert.AreEqual(2, await dbCars.CountAsync());
            Car theCar = dbCars.Find(1);
            Assert.AreEqual("Chevy", theCar.Make);
            Assert.AreEqual("Impala", theCar.Model);
            theCar = dbCars.Find(2);
            Assert.AreEqual(2, theCar.Id);
            Assert.AreEqual(2015, theCar.Year);
        }
    }
}
